<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**店家相關 */
Route::get('/shop/about', 'Shop\Basic\ShopBasicController@aboutShop');

/**商品瀏覽 */
Route::get('/product', 'Product\Basic\ProductBasicController@displayProducts');

/**商品細項展示 */
Route::get('/product/{ID}', 'Product\Basic\ProductBasicController@displayProductDetail');

/**花園新鮮事總覽 */
Route::get('/news', 'Shop\News\ShopNewsController@displayNews');

/**花園新鮮事細項展示 */
Route::get('/news/{ID}', 'Shop\News\ShopNewsController@displayNewsDetail');

/**店家聯絡/問題詢問 */
Route::get('/shop/contact', 'Shop\Basic\ShopBasicController@contactWithShop');